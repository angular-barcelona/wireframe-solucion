# Wireframe

1. Instala las dependencias del proyecto.
2. Lanza la aplicación en el navegador.
3. Crea mediante Angular CLI los módulos necesarios:  
`ng generate module <nombre-modulo>`
4. Crea mediante Angular CLI los componentes necesarios (al final del enunciado tienes la correspondencia entre cada componente y su módulo):  
`ng generate component <carpeta-modulo>/<nombre-componente>`
5. En el template de AppComponent encontrarás comentarios que te indicarán el HTML que corresponde a cada componente. Lleva cada trozo de código al template de su código correspondiente, y en su lugar escribe la etiqueta HTML del componente para decirle a Angular que lo renderice ahí.
6. Haz todo lo necesario para que la aplicación se vea exactamente igual que antes de dividir en componentes (y sin errores en la consola).


### Correspondencia componentes-módulos

CabeceraComponent -> CoreModule  
WidgetTransferenciaComponent -> TraficoModule  
WidgetEspacioComponent -> HDModule  
WidgetBuzonesComponent -> EmailModule  
WidgetFTPComponent -> FTPModule  
WidgetBDComponent -> BDModule  
WidgetFireWallComponent -> SeguridadModule  
BotonesAccionesComponent -> AdminModule
