import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetBDComponent } from './widget-bd/widget-bd.component';
import { AdminModule } from '../admin/admin.module';

@NgModule({
  declarations: [WidgetBDComponent],
  imports: [
    CommonModule,
    AdminModule
  ],
  exports: [
    WidgetBDComponent
  ]
})
export class BDModule { }
