import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { TraficoModule } from './trafico/trafico.module';
import { HDModule } from './hd/hd.module';
import { EmailModule } from './email/email.module';
import { FTPModule } from './ftp/ftp.module';
import { BDModule } from './bd/bd.module';
import { SeguridadModule } from './seguridad/seguridad.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CoreModule,
    TraficoModule,
    HDModule,
    EmailModule,
    FTPModule,
    BDModule,
    SeguridadModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
