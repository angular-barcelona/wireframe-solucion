import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetEspacioComponent } from './widget-espacio/widget-espacio.component';
import { AdminModule } from '../admin/admin.module';

@NgModule({
  declarations: [WidgetEspacioComponent],
  imports: [
    CommonModule,
    AdminModule
  ],
  exports: [WidgetEspacioComponent]
})
export class HDModule { }
