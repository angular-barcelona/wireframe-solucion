import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetFirewallComponent } from './widget-firewall/widget-firewall.component';
import { AdminModule } from '../admin/admin.module';

@NgModule({
  declarations: [WidgetFirewallComponent],
  imports: [
    CommonModule,
    AdminModule
  ],
  exports: [WidgetFirewallComponent]
})
export class SeguridadModule { }
