import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BotonesAccionesComponent } from './botones-acciones/botones-acciones.component';

@NgModule({
  declarations: [BotonesAccionesComponent],
  imports: [
    CommonModule
  ],
  exports: [BotonesAccionesComponent]
})
export class AdminModule { }
