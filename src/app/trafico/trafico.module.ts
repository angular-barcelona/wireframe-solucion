import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetTransferenciaComponent } from './widget-transferencia/widget-transferencia.component';
import { AdminModule } from '../admin/admin.module';

@NgModule({
  declarations: [WidgetTransferenciaComponent],
  imports: [
    CommonModule,
    AdminModule
  ],
  exports: [WidgetTransferenciaComponent]
})
export class TraficoModule { }
