import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetBuzonesComponent } from './widget-buzones/widget-buzones.component';
import { AdminModule } from '../admin/admin.module';

@NgModule({
  declarations: [WidgetBuzonesComponent],
  imports: [
    CommonModule,
    AdminModule
  ],
  exports: [WidgetBuzonesComponent]
})
export class EmailModule { }
