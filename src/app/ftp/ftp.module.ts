import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WidgetFTPComponent } from './widget-ftp/widget-ftp.component';
import { AdminModule } from '../admin/admin.module';

@NgModule({
  declarations: [WidgetFTPComponent],
  imports: [
    CommonModule,
    AdminModule
  ],
  exports: [
    WidgetFTPComponent
  ]
})
export class FTPModule { }
